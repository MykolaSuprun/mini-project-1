package com.example.mini_project_1;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class ListActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<Product> productList;

    DatabaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        boolean darkTheme = loadTheme();
        setTheme(darkTheme ? R.style.AppThemeDark : R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        productList = new ArrayList<>();

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new ProductAdapter(this, productList);
        recyclerView.setAdapter(adapter);

        myDb = new DatabaseHelper(this);
        Cursor cursor = myDb.getAllData();
        if (cursor.getCount() == 0)
            Toast.makeText(ListActivity.this, "Your list is empty!", Toast.LENGTH_LONG)
                    .show();

        StringBuffer productNameBuffer = new StringBuffer();
        StringBuffer productPriceBuffer = new StringBuffer();
        StringBuffer productQuantityBuffer = new StringBuffer();

        while (cursor.moveToNext()) {
            productNameBuffer.append(cursor.getString(1) + "/n");
            productPriceBuffer.append(cursor.getString(2) + "/n");
            productQuantityBuffer.append(cursor.getString(3) + "/n");
        }

        String[] productNames = stringToStringArray(productNameBuffer.toString());
        double[] productPrices = stringToDoubleArray(productPriceBuffer.toString());
        int[] productQuantity = stringToIntArray(productQuantityBuffer.toString());

        for (int i = 0; i < productNames.length; i++) {
            productList.add(new Product(productNames[i], productPrices[i], productQuantity[i]));
        }

    }

    public String[] stringToStringArray(String string) {
        Pattern pattern = Pattern.compile("/n");
        String[] elements = pattern.split(string);
        return elements;
    }

    public double[] stringToDoubleArray(String string) {
        Pattern pattern = Pattern.compile("/n");
        String[] elements = pattern.split(string);
        double[] doubleElements = new double[elements.length];

        for (int i = 0; i < elements.length; i++)
            doubleElements[i] = Double.parseDouble(elements[i]);

        return doubleElements;
    }

    public int[] stringToIntArray(String string) {
        Pattern pattern = Pattern.compile("/n");
        String[] elements = pattern.split(string);
        int[] intElements = new int[elements.length];

        for (int i = 0; i < elements.length; i++)
            intElements[i] = Integer.parseInt(elements[i]);

        return intElements;
    }

    public boolean loadTheme() {
        SharedPreferences sharedPreferences = getSharedPreferences(SharedPrefs.
                SHARED_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(SharedPrefs.PREF_TEXT, false);

    }
}
