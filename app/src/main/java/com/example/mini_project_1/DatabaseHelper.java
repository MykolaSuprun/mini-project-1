package com.example.mini_project_1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "products.db";
    public static final String TABLE_NAME = "product_table";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "PRODUCT_NAME";
    public static final String COL_3 = "PRICE";
    public static final String COL_4 = "QUANTITY";


    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                " PRODUCT_NAME TEXT, PRICE DOUBLE, QUANTITY INTEGER) ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(Product product) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, product.getName());
        contentValues.put(COL_3, product.getPrice());
        contentValues.put(COL_4, product.getQuantity());
        long result = db.insert(TABLE_NAME, null, contentValues);

        return result != -1;
    }

    public Integer removeRowByName(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, COL_2 + " = ?", new String[]{name});
    }

    public void removeAllProducts(SQLiteDatabase db) {
        db.execSQL("DELETE FROM " + TABLE_NAME);
    }

    public boolean updateRow(Product product){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, product.getName());
        contentValues.put(COL_3, product.getPrice());
        contentValues.put(COL_4, product.getQuantity());

        long result = db.update(TABLE_NAME, contentValues,COL_2 + " = ?", new String[]{
                product.getName()
        });

        return result != -1;
    }

    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME
                + " WHERE " + COL_2 + " IS NOT NULL AND " +
                COL_3 + " IS NOT NULL AND " +
                COL_4 + " IS NOT NULL" + " ORDER BY " +
                COL_3 + " DESC", null);
        return cursor;
    }

}
