package com.example.mini_project_1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class add_product extends AppCompatActivity {

    DatabaseHelper myDb;
    EditText editProductName, editPrice, editQuantity;
    Button addProductButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        boolean darkTheme = loadTheme();
        setTheme(darkTheme ? R.style.AppThemeDark : R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        myDb = new DatabaseHelper(this);

        editProductName = findViewById(R.id.addProductNameEdit);
        editPrice = findViewById(R.id.addProductPriceEdit);
        editQuantity = findViewById(R.id.addProductQuantityEdit);
        addProductButton = findViewById(R.id.addProductButton);
    }

    public void addProductToDataBase(View view) {
        String productName = editProductName.getText().toString();
        String priceStr = editPrice.getText().toString();
        String quantityStr = editQuantity.getText().toString();
        double price;
        int quantity;

        if (isDouble(priceStr))
            price = Double.parseDouble(priceStr);
        else {
            editPrice.setText("Enter a number");
            return;
        }

        if (android.text.TextUtils.isDigitsOnly(quantityStr))
            quantity = Integer.parseInt(quantityStr);
        else {
            editQuantity.setText("Enter a number");
            return;
        }
        boolean isInserted = myDb.insertData(new Product(productName, price, quantity));

        if (isInserted)
            Toast.makeText(add_product.this, "Product was added", Toast.LENGTH_LONG)
                    .show();
        else
            Toast.makeText(add_product.this, "Product addition failed", Toast.LENGTH_LONG)
                    .show();
    }

    public static boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void removeProduct(View view) {
        Integer deletedRows = myDb.removeRowByName(editProductName.getText().toString());
        if (deletedRows > 0)
            Toast.makeText(add_product.this, "Product deleted", Toast.LENGTH_LONG)
                    .show();
        else
            Toast.makeText(add_product.this, "No such product", Toast.LENGTH_LONG)
                    .show();

    }


    public boolean loadTheme() {
        SharedPreferences sharedPreferences = getSharedPreferences(SharedPrefs.
                SHARED_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(SharedPrefs.PREF_TEXT, false);

    }

    public void updateProductClick(View view) {

        String productName = editProductName.getText().toString();
        String priceStr = editPrice.getText().toString();
        String quantityStr = editQuantity.getText().toString();
        double price;
        int quantity;

        if (isDouble(priceStr))
            price = Double.parseDouble(priceStr);
        else {
            editPrice.setText("Enter a number");
            return;
        }

        if (android.text.TextUtils.isDigitsOnly(quantityStr))
            quantity = Integer.parseInt(quantityStr);
        else {
            editQuantity.setText("Enter a number");
            return;
        }
        boolean isInserted = myDb.updateRow(new Product(productName, price, quantity));

        if (isInserted)
            Toast.makeText(add_product.this, "Product was updated", Toast.LENGTH_LONG)
                    .show();
        else
            Toast.makeText(add_product.this, "Product update failed", Toast.LENGTH_LONG)
                    .show();

    }
}
