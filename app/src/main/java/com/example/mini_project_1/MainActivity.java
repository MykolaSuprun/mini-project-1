package com.example.mini_project_1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.audiofx.Equalizer;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    ConstraintLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        boolean darkTheme = loadTheme();
        setTheme(darkTheme ? R.style.AppThemeDark : R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        layout = findViewById(R.id.mainLayout);

        if (loadBackground())
            layout.setBackgroundColor(Color.parseColor("#8bae1d"));

    }

    public void listButtonClick(View view) {
        Intent listIntent = new Intent(this, ListActivity.class);
        startActivity(listIntent);
    }

    public void settingsButtonClick(View view) {
        Intent settingsIntent = new Intent(this, SettingsActivity.class);
        startActivity(settingsIntent);
    }

    public void addProductClick(View view) {
        Intent settingsIntent = new Intent(this, add_product.class);
        startActivity(settingsIntent);
    }

    public boolean loadTheme() {
        SharedPreferences sharedPreferences = getSharedPreferences(SharedPrefs.
                SHARED_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(SharedPrefs.PREF_TEXT, false);

    }

    public boolean loadBackground() {
        SharedPreferences sharedPreferences = getSharedPreferences(SharedPrefs.
                SHARED_PREFS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(SharedPrefs.PREF_BACKGROUND, false);
    }
}
